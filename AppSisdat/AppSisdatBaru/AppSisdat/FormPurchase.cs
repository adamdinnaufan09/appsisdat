﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AppSisdat
{
    public partial class FormPurchase : Form
    {
        Koneksi konn = new Koneksi();
        private SqlCommand cmd;
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader rd;

        void KondisiAwal()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            MunculDataPurchase();
        }

        public FormPurchase()
        {
            InitializeComponent();
        }

        void MunculDataPurchase()
        {

            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("select * from Purchase", conn);
            ds = new DataSet();
            da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Purchase");
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Purchase";
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Refresh();
        }

        void CariPurchase()
        {
            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("select * from Purchase where Id_Purchase like '%" + textBox6.Text + "%'", conn);
            ds = new DataSet();
            da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Purchase");
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Purchase";
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Refresh();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormPurchase_Load(object sender, EventArgs e)
        {
            KondisiAwal();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("select * from Purchase where Id_purchase = '" + textBox1.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    textBox1.Text = rd[0].ToString();
                    textBox2.Text = rd[1].ToString();
                    textBox3.Text = rd[2].ToString();
                    textBox4.Text = rd[3].ToString();
                    dateTimePicker1.Value = rd[4].ToString();
                }
                else
                {
                    MessageBox.Show("Data Tidak Ada!");
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "" || textBox3.Text.Trim() == "" || textBox4.Text.Trim() == "" || dateTimePicker1.Value.ToString().Trim() == "")
            {
                MessageBox.Show("Pastikan Semua Field Terisi!");
            }
            else
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("insert into Purchase values ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + dateTimePicker1.Value.ToString() + "')", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Berhasil diinput");
                KondisiAwal();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "" || textBox3.Text.Trim() == "" || textBox4.Text.Trim() == "" || dateTimePicker1.Value.ToString().Trim() == "")
            {
                MessageBox.Show("Pastikan Semua Field Terisi!");
            }
            else
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("update Customer set Id_customer='" + textBox2.Text + "', Id_model='" + textBox3.Text + "', Id_employee='" + textBox4.Text + "', tanggal_beli='" + dateTimePicker1.Value + "'where Id_purchase='" + textBox1.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Berhasil diupdate");
                KondisiAwal();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "" || textBox3.Text.Trim() == "" || textBox4.Text.Trim() == "" || dateTimePicker1.Value.ToString().Trim() == "")
            {
                MessageBox.Show("Pastikan Semua Field Terisi!");
            }
            else
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("delete Purchase where Id_purchase='" + textBox1.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Berhasil didelete");
                KondisiAwal();
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            CariPurchase();
        }
    }
}
