﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSisdat
{
    public partial class FormCustomerOrder : Form
    {
        Koneksi konn = new Koneksi();
        private SqlCommand cmd;
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader rd;

        public FormCustomerOrder()
        {
            InitializeComponent();
        }

        void KondisiAwal()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            MunculDataCustomerOrder();
        }

        void FormCustomerOrder_Load(object sender, EventArgs e)
        {
            KondisiAwal();
        }

        void MunculDataCustomerOrder()
        {
            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("select * from Customer_Order", conn);
            ds = new DataSet();
            da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Customer_Order");
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Customer_Order";
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void CariIdCustomer()
        {
            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("select * from Customer_Order where Id_customer like '%" + textBox6.Text + "%' or Id_model like '" + textBox6.Text + "%'", conn);
            ds = new DataSet();
            da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Customer_Order");
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Customer_Order";
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Refresh();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("select * from Customer_Order where Id_customer = '" + textBox1.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    textBox1.Text = rd[0].ToString();
                    textBox2.Text = rd[1].ToString();
                }
                else
                {
                    MessageBox.Show("Data Tidak Ada!");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "")
            {
                MessageBox.Show("Pastikan Semua Field Terisi!");
            }
            else
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("insert into Customer_Order values ('" + textBox1.Text + "','" + textBox2.Text +"')", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Berhasil diinput");
                KondisiAwal();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "")
            {
                MessageBox.Show("Pastikan Semua Field Terisi!");
            }
            else
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("update Customer_Order set Id_model='" + textBox2.Text + "' where Id_customer='" + textBox1.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Berhasil diupdate");
                KondisiAwal();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "")
            {
                MessageBox.Show("Pastikan Semua Field Terisi!");
            }
            else
            {
                SqlConnection conn = konn.GetConn();

                cmd = new SqlCommand("delete Customer_Order where Id_customer='" + textBox1.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data Berhasil didelete");
                KondisiAwal();
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            CariIdCustomer();
        }

        private void FormCustomerOrder_Load_1(object sender, EventArgs e)
        {
            KondisiAwal();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
